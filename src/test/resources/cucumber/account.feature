Feature: Account
  As a transfer-initiator
  I want to initiate a transfer
  So that I can send money between accounts

  Scenario: Send funds
    Given A source account with a balance of 250.00
    And a valid target account
    When I send 50.00
    Then the source should have a balance of 200.00
    And the target should have a balance of 50.00

  Scenario: Send with insufficient funds
    Given A source account with a balance of 0.00
    And a valid target account
    When I send 50.00
    Then the account should throw an insufficient funds exception

  Scenario: Receive money from an unregistered account
    Given An unregistered source account
    And a valid target account
    When I send 50.00
    Then the account manager should throw an no such account exception