package london.jsmith.revolut.models.transfer;

/**
 * Created by Jim Smith on 25/10/2017.
 */
public class MockPaymentOrder extends PaymentOrder {

    public MockPaymentOrder(double value, String sourceIBAN) {
        super(Direction.RECEIVE, value, sourceIBAN);
    }

}
