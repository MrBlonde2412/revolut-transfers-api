package london.jsmith.revolut.models.account;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

/**
 * Created by Jim Smith on 05/11/2017.
 */
public class AccountsManagerTest {

    private AccountsManager manager;

    @Before
    public void setUp() throws Exception {
        manager = new AccountsManager();
    }

    @Test
    public void open() throws Exception {
        Account account = manager.open();
        Assert.assertNotNull( account );
        Assert.assertEquals( 0, account.getBalance(), 0 );
    }

    @Test
    public void find() throws Exception {
        Account newAccount = manager.open();
        Account lookedUpAccount = manager.find(newAccount.getIBAN());
        Assert.assertEquals(newAccount, lookedUpAccount);
    }

    @Test
    public void newUniqueIBAN() throws Exception {
        int capacity = 10000;

        Set ibanSet = new HashSet(capacity);
        IntStream.range(0, capacity).forEach( i -> ibanSet.add( AccountsManager.newUniqueIBAN() ) );
        Assert.assertEquals( capacity, ibanSet.size() );
    }

}