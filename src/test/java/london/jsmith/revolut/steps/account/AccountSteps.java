package london.jsmith.revolut.steps.account;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import london.jsmith.revolut.exceptions.InsufficientFundsException;
import london.jsmith.revolut.exceptions.NoSuchAccountException;
import london.jsmith.revolut.models.transfer.MockPaymentOrder;
import london.jsmith.revolut.models.account.AccountsManager;
import london.jsmith.revolut.models.transfer.Coordinator;
import london.jsmith.revolut.models.transfer.PaymentOrder;
import london.jsmith.revolut.models.transfer.TransferRequest;

import static org.junit.Assert.assertTrue;

public class AccountSteps {

    private AccountsManager accountsManager;
    private Coordinator coordinator;
    private Exception exception;
    private TransferRequest badRequest;
    private String sourceAddress;
    private String targetAddress;

    @Before
    public void setUp() {
        accountsManager = new AccountsManager();
        coordinator = new Coordinator(accountsManager);
    }

    @Given("^A source account with a balance of (.*?)$")
    public void a_source_account_with_substantial_funds(double value) throws Throwable {
        sourceAddress = accountsManager.open().getIBAN();
        PaymentOrder openingFunds = new MockPaymentOrder(value, "my-test-IBAN");
        accountsManager.find(sourceAddress).receive(openingFunds);
    }


    @And("^a valid target account$")
    public void aValidTargetAccount() throws Throwable {
        targetAddress = accountsManager.open().getIBAN();
    }

    @When("^I send (.*?)$")
    public void iSend(double value) throws Throwable {
        TransferRequest mockRequest = new TransferRequest(value, sourceAddress, targetAddress);
        try { coordinator.execute(mockRequest); }
        catch (Exception e) {exception = e;}
    }

    @Then("^the source should have a balance of (.*?)$")
    public void theSourceShouldHaveABalanceOf(double value) throws Throwable {
        assertTrue(accountsManager.find(sourceAddress).getBalance() == value);
    }

    @And("^the target should have a balance of (.*?)$")
    public void theTargetShouldHaveABalanceOf(double value) throws Throwable {
        assertTrue(accountsManager.find(targetAddress).getBalance() == value);
    }

    @Then("^the account should throw an insufficient funds exception$")
    public void theAccountShouldThrowAnInsufficientFundsException() throws Throwable {
        assertTrue( exception instanceof InsufficientFundsException );
    }

    @Then("^the account manager should throw an no such account exception$")
    public void theAccountShouldThrowAnNoSuchAccountException() throws Throwable {
        assertTrue( exception instanceof NoSuchAccountException );
    }

    @Given("^An unregistered source account$")
    public void anUnregisteredSourceAccount() throws Throwable {
        sourceAddress = "a-fake-iban";
    }


}
