package london.jsmith.revolut;

/**
 * Created by Jim Smith on 24/10/2017.
 */
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        glue = "london.jsmith.revolut.steps.account",
        features = "classpath:cucumber/account.feature"
)
public class AccountStepsTest {}