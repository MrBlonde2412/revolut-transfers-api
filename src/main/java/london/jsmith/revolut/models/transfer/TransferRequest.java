package london.jsmith.revolut.models.transfer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.h2.value.Transfer;

/**
 * When an initiator wishes to transfer funds from one account to another, they will instantiate a TransferRequest.
 * A TransferRequest is the raw expression of desire to move funds from one account to another, and as such it may be rejected
 * in many cases.
 *
 * @author Jim Smith
 */
@DatabaseTable(tableName = "transfer_requests")
public class TransferRequest {
    @DatabaseField(canBeNull = false)private double value;
    @DatabaseField(canBeNull = false)private String target;
    @DatabaseField(canBeNull = false)private String source;

    TransferRequest(){}

    @JsonCreator
    public TransferRequest(
            @JsonProperty("value") double value,
            @JsonProperty("sourceIBAN") String sourceIBAN,
            @JsonProperty("targetIBAN") String targetIBAN) {

        this.value = value;
        this.target = targetIBAN;
        this.source = sourceIBAN;
    }

    public double getValue() {
        return value;
    }

    public String getTarget() {
        return target;
    }

    public String getSource() {
        return source;
    }
}
