package london.jsmith.revolut.models.transfer;

public enum Direction {
    SEND, RECEIVE
}
