package london.jsmith.revolut.models.transfer;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;
import london.jsmith.revolut.App;
import london.jsmith.revolut.exceptions.InsufficientFundsException;
import london.jsmith.revolut.models.account.Account;
import london.jsmith.revolut.models.account.AccountsManager;
import london.jsmith.revolut.exceptions.NoSuchAccountException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * The Coordinator actor is perhaps the most important in the transfer sequence. It takes responsibility for validating the
 * TransferRequest when it comes in, splitting it into a PaymentOrder and a SendOrder - which will be received by the source and target
 * accounts respectively.
 */
public class Coordinator {

    private AccountsManager accountManager;
    private Dao<TransferRequest, String> requestDao;
    private Logger logger = Logger.getLogger(Coordinator.class);

    public Coordinator(AccountsManager accountManager) {
        this.accountManager = accountManager;
        try {
            requestDao = DaoManager.createDao(App.CONNECTION, TransferRequest.class);
            TableUtils.createTableIfNotExists(App.CONNECTION, TransferRequest.class);
        } catch (SQLException e) {logger.error(e);}
    }

    public void execute(TransferRequest request) throws InsufficientFundsException, NoSuchAccountException {
        try {requestDao.create(request);}
        catch (SQLException e) {logger.error(e);}
        Pair<PaymentOrder, PaymentOrder> orders = split(request);
        Account source = accountManager.find(request.getSource());
        Account target = accountManager.find(request.getTarget());
        source.pay(orders.getLeft());
        target.receive(orders.getRight());
    }

    final Pair<PaymentOrder, PaymentOrder> split(TransferRequest request){
        PaymentOrder send = new PaymentOrder(Direction.SEND, request.getValue(), request.getTarget() );
        PaymentOrder receive = new PaymentOrder(Direction.RECEIVE, request.getValue(), request.getSource() );
        return new ImmutablePair<>(send, receive);
    }

    public AccountsManager getAccountManager() {
        return accountManager;
    }
}
