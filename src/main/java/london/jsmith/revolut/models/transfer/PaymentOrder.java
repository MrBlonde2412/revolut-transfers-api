package london.jsmith.revolut.models.transfer;

/**
 * A PaymentOrder is a message sent by a Coordinator to take funds from another Account. It has a value and a source.
 * In Exceptional circumstance, an Account may decide to reject the Order, perhaps because the Account is invalid - however
 * most pre-transfer checks will be carried out by the Coordinator.
 *
 * @author Jim Smith
 */
public class PaymentOrder {

    private double value;
    private String target;

    PaymentOrder(Direction direction, double value, String target) {
        this.value = value;
        this.target = target;
    }

    public double getValue() {
        return value;
    }

    public String getTarget() {
        return target;
    }
}
