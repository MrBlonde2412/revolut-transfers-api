package london.jsmith.revolut.models.account;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;
import london.jsmith.revolut.App;
import london.jsmith.revolut.exceptions.NoSuchAccountException;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.UUID;

/**
 * An AccountsManager is responsible for handling the implementation of creating and storing accounts. It is the entry-point
 * to an Account object.
 *
 * @author Jim Smith
 */
public class AccountsManager {

    private Dao<Account, String> accountsDao;
    private static Logger logger = Logger.getLogger(AccountsManager.class);

    public AccountsManager() {
        try {
            this.accountsDao = DaoManager.createDao(App.CONNECTION, Account.class);
            TableUtils.createTableIfNotExists(App.CONNECTION, Account.class);
        }
        catch (SQLException e) {logger.error(e);}
    }

    public Account open(){
        Account account = new Account();
        try {accountsDao.create(account);}
        catch (SQLException e) {logger.error(e);}
        return account;
    }

    public Account find(String IBAN) throws NoSuchAccountException {
        try {
            Account account = accountsDao.queryForId(IBAN);
            if (account == null) throw new NoSuchAccountException(IBAN);
            return account;
        } catch(Exception e){ throw new NoSuchAccountException(e); }
    }

    static final String newUniqueIBAN(){
        return UUID.randomUUID().toString();
    }

}
