package london.jsmith.revolut.models.account;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import london.jsmith.revolut.App;
import london.jsmith.revolut.exceptions.InsufficientFundsException;
import london.jsmith.revolut.models.transfer.PaymentOrder;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * A simple POJO for an account. It has an identifier (IBAN) and a balance.
 * @author Jim Smith.
 */
@DatabaseTable(tableName="accounts")
public class Account {

    @DatabaseField(columnName = "iban", canBeNull = false, id = true)
    private String IBAN;
    @DatabaseField
    private double balance;
    private Dao<Account, String> dao;
    private static Logger logger = Logger.getLogger(Account.class);

    Account(){
        try {this.dao = DaoManager.createDao(App.CONNECTION, Account.class);}
        catch (SQLException e) {logger.error(e);}
        this.IBAN = AccountsManager.newUniqueIBAN();
    }

    public void receive(PaymentOrder paymentOrder) {
        logger.info("receiving funds");
        this.balance += paymentOrder.getValue();
        try {dao.update(this);}
        catch (SQLException e) {logger.error(e);}
    }

    public void pay(PaymentOrder sendOrder) throws InsufficientFundsException {
        logger.info("paying funds");
        if (sendOrder.getValue() > balance) throw new InsufficientFundsException();
        else {
            this.balance -= sendOrder.getValue();
            try {dao.update(this);}
            catch (SQLException e) {logger.error(e);}
        }
    }

    public double getBalance() {
        return balance;
    }

    public String getIBAN() {
        return IBAN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        if (Double.compare(account.balance, balance) != 0) return false;
        return IBAN.equals(account.IBAN);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = IBAN.hashCode();
        temp = Double.doubleToLongBits(balance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
