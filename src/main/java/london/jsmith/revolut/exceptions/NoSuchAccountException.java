package london.jsmith.revolut.exceptions;

import java.util.NoSuchElementException;

/**
 * Created by Jim Smith on 29/10/2017.
 */
public class NoSuchAccountException extends Exception {

    public NoSuchAccountException(){
        super();
    }

    public NoSuchAccountException(String IBAN){
        super(IBAN);
    }

    public NoSuchAccountException(Throwable throwable){
        super(throwable);
    }

    public NoSuchAccountException(String IBAN, Throwable throwable){
        super(IBAN, throwable);
    }

}
