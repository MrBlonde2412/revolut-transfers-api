package london.jsmith.revolut.exceptions;

/**
 * Created by Jim Smith on 29/10/2017.
 */
public class InsufficientFundsException extends Exception {
    public InsufficientFundsException(){
        super("Insufficient funds");
    }
}
