package london.jsmith.revolut.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import london.jsmith.revolut.exceptions.InsufficientFundsException;
import london.jsmith.revolut.exceptions.NoSuchAccountException;
import london.jsmith.revolut.models.account.Account;
import london.jsmith.revolut.models.account.AccountsManager;
import london.jsmith.revolut.models.transfer.Coordinator;
import london.jsmith.revolut.models.transfer.TransferRequest;
import org.apache.log4j.Logger;
import spark.Request;
import spark.Response;

import java.io.IOException;

public class TransferHandler {

    private static Logger logger = Logger.getLogger(TransferHandler.class);
    private ObjectMapper mapper = new ObjectMapper();
    private AccountsManager manager;
    private Coordinator coordinator;

    public TransferHandler(Coordinator coordinator){
        this.coordinator = coordinator;
        this.manager = coordinator.getAccountManager();
    }

    // TODO : refactor to use common response format
    public JsonNode openAccount(Request request, Response response) {
        logger.info("new account request");
        ObjectNode root = mapper.createObjectNode();
        root.put("message", "account creation successful");
        root.put("status","success");
        root.put("IBAN", manager.open().getIBAN());
        response.status(ResponseCodes.CREATED);
        response.type("application/json");
        return root;
    }

    // TODO : refactor to use common response format
    public JsonNode transfer(Request request, Response response) {
        logger.info("new transfer request received");
        ObjectNode root = mapper.createObjectNode();
        TransferRequest transferRequest = null;
        try {
            transferRequest = mapper.readValue(request.body(), TransferRequest.class);
            coordinator.execute(transferRequest);
            root.put("status", "submitted");
            response.status(ResponseCodes.ACCEPTED);
        } catch (IOException | InsufficientFundsException | NoSuchAccountException e) {
            root.put("status", "failed");
            response.status(ResponseCodes.BAD_REQUEST);
            root.put("message", e.getLocalizedMessage());
            logger.error(e);
        }
        response.type("application/json");
        return root;
    }

    // TODO : refactor to use common response format
    public JsonNode lookup(Request request, Response response){
        logger.info("new lookup request received");
        String iban = request.params("iban");
        ObjectNode root = null;
        response.type("application/json");
        response.status(ResponseCodes.OK);
        try {
            Account account = manager.find( iban );
            return mapper.convertValue(account, JsonNode.class);
        } catch (NoSuchAccountException e) {
            root = mapper.createObjectNode();
            response.status(ResponseCodes.NO_CONTENT);
            root.put("status", "failed");
            root.put("message", "couldn't find account with IBAN " + iban);
            logger.error(e);
        }
        return root;
    }
}
