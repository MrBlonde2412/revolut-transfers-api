package london.jsmith.revolut.controllers;

/**
 * Just a container for the response code constants.
 * @author Jim Smith
 */
public class ResponseCodes {

    static final int OK = 200;
    static final int CREATED = 201;
    static final int ACCEPTED = 202;
    static final int NO_CONTENT = 204;
    static final int BAD_REQUEST = 400;
    static final int SERVER_ERROR = 500;

    private ResponseCodes(){}

}
