package london.jsmith.revolut;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import london.jsmith.revolut.controllers.TransferHandler;
import london.jsmith.revolut.models.account.AccountsManager;
import london.jsmith.revolut.models.transfer.Coordinator;
import org.apache.log4j.Logger;
import java.sql.SQLException;
import static spark.Spark.*;

/**
 * The entry point to the Transfer API Application. When run, the class will initialise the database connection, create the
 * API endpoints, and bind them to the controller actins.
 *
 * @author Jim Smith
 */
public class App {
    public static JdbcConnectionSource CONNECTION;
    private static Logger logger = Logger.getLogger(App.class);
    private static Config config;

    static {
        config = ConfigFactory.load();
        Config persistenceConfig = config.getConfig("persistence");
        try {
            Class.forName(persistenceConfig.getString("driverClassName"));
            CONNECTION = new JdbcConnectionSource(persistenceConfig.getString("connectionString"));
        }
        catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        AccountsManager manager = new AccountsManager();
        Coordinator coordinator = new Coordinator(manager);
        TransferHandler handler = new TransferHandler(coordinator);
        path("/api", () -> {
            before("/*", (request, response) -> logger.info("Received api call"));
            post("/account", handler::openAccount);
            get("/account/:iban", handler::lookup);
            post("/transfer", handler::transfer);
        });
    }
}
