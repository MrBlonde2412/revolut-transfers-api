Transfers API
==================
A simple implementation of a RESTful payment transfers API built with [Spark](https://sparktutorials.github.io/),
[H2](http://h2database.com/html/main.html), and [Cucumber](https://cucumber.io/docs/reference/jvm). 


**Quickstart**

Build with `mvn clean install`. Run with `java -jar .\target\transfersapi-1.0-SNAPSHOT.jar`, runs on port 4567. The functionality is mostly described via the 
cucumber test scripts, which can be inspected by running `mvn test`.

**Endpoints**

`POST /api/account` Create a new account.

 ---

`GET /api/account/:IBAN`  Get account by IBAN.
 
 ---
`
POST /api/transfer`  Transfer funds from one account to another by IBAN

payload: 
```json 
{"souce":"my_source_iban", "target":"my_target_iban","value":10.00}
```



**Assumptions**
* Accounts can be identified by IBAN alone, no account number or sort code required.
* We don't need SSL or any other kind of encryption.

**Design**

The behavioural model can be summarised by three actors; the `Initiator`, `Coordinator`, `AccountManager`, and the `Account`.
An initiator is anything which sends a post request to `/api/transfer`, thus initiating the transfer. This could be a person, via a 
browser, or an internal system via a REST client. The Coordinator is responsible for interpreting the transfer request, checking that the transaction is legal, and executing.
The Account Manager is the entry point to the Accounts. It can open a new account or find existing ones, and as such has access to
the persistence mechanism. 

![Sequence](docs/sequence.png)

Sequence

![Data](docs/data.png)

Data model

**TODO**
* Refactor to asynchronous, look into other RESTful micro-frameworks.
* Authentication / rules around transferring funds, perhaps token-based.
* Persistence of `TransferRequests` and `Orders`
* Refactor the controller layer to insulate business logic from REST interface.
* Concurrency strategy.
